﻿Try
{
    # Import the gateway file module. Temporarily force the module to be added
    Remove-Module -name "gateway-file-generaor" 
    Import-Module "$(Split-Path -parent $PSCommandPath)\gateway-file-generaor.psm1" -Force
    
    Start-RuleBasedGatewayScript `
    -gatewayFileFormat "santander" `
    -gatewayFileLimit 10 `
    -CsvProcessLimit 10 `
    -clientHeaderName "santander" `
    -clientCsvName "santander-#1-10000cases.csv" `
    -delimiterTabbed $false `
    -clientGatewayMappingName "santander-gateway-mapping.csv" `
    -gatewayRulesName "Gateway-Merge-Rules.csv" `
    -logToHost $true
}
Catch
{
    Write-Host $_.GetType()
    Write-Host $_.Exception
    Write-Host $_.Exception.StackTrace
    throw 
}