﻿# ---------------------------------------------------------------------
# The CSV headers represent cases exported into a spreadsheet from 
# a pervasive database. Each client will require a tailored header 
# function within the Gateway file module.
# ---------------------------------------------------------------------
function Import-CsvWithHeaders
{
    param(
        [Parameter(mandatory=$true)]
        [string] $thePath,
        [Parameter(mandatory=$false)]
        [bool] $isTabbed = $false
    )

    if($isTabbed)
    {
        $delimiter = "`t"
    }else
    {
        $delimiter = ","
    }

    switch($headerName)
    {
        "barclays"
        {
        	# Supply a unique column id and specify each column
            [Array]$theData = Import-Csv $thePath -Header Ignore1,Ignore2,Case.EnteredDate1,Property.Address.Street,Property.Address.Town,Property.Address.County,Property.Address.Number,Property.Address.Postcode,Case.EnteredDate2,Case.LastModifiedDate,Instruction.RawTypeCode,Empty.Field11,Property.Address.Area,Invoice.GrossFee,Case.CompletedDate,Case.SysRef,Empty.Field16,Empty.Field17,Invoice.FeeNotes0,Form.TypedBy,Applicants0.Address.Name,Applicants0.Address.Number,Applicants0.Address.Street,Applicants0.Address.Area,Applicants0.Address.Town,Applicants0.Address.County,Applicants0.Address.Postcode,Case.LenderReference,Case.SpnIn,Property.UnexpiredTerm,Property.GroundRent,Case.AdminNotes1,EstateAgent.AccessDetails,Case.AdminNotes2,Property.RawPropertyType,Case.AdminNotes3,Case.AdminNotes4,Applicants0.Title,Applicants0.Initials,Property.Price,Surveyor.staffArea,Valuation.InspectionDate,Case.Error,Property.RawTenureType,EstateAgent.Name,Empty.Field45,EstateAgent.Telephone,Empty.Field47,Instruction.LoanAmount,Property.YearBuilt,Empty.Field50,Property.Bedrooms,Property.ReceptionRooms,Empty.Field53,Case.PanelDate,Empty.Field55,Empty.Field56,Applicants1.Title,Applicants1.Initials,Surveyor.Group,Empty.Field60,Property.IsNewBuild,Property.IsListedBuilding,Empty.Field63,Empty.Field64,Case.StatusCode,Surveyor.Code,Empty.Field67,Property.RawConditionType,Property.Address.Name,Client.SourceBranchName,Property.BlockFloorCount,Property.RawParkingType,Empty.Field73,Applicants0.DaytimeTelephone,Instruction.Lender,Property.RawStyle,Property.Vendor.EveningTelephone,Case.Forms,Applicants0.EveningTelephone,Empty.Field80,Empty.Field81,Property.FloorArea,Empty.Field83,Empty.Field84,Property.Vendor.DaytimeTelephone,Valuation.Notes0,Empty.Field87,Invoice.netFeeVAT,Empty.Field89,Empty.Field90,Property.Vendor.Title,Property.Vendor.Initials,Property.Vendor.Surname,Property.RawPriceType,Property.RawCentralHeating,Invoice.netFee,Invoice.ScaleFee,Instruction.SourceSystemCode,Empty.Field99,Applicants0.Surname,Applicants1.Surname,Client.ReturnAddress.Name,Client.ReturnAddress.Number,Client.ReturnAddress.Street,Client.ReturnAddress.Area,Client.ReturnAddress.Town,Client.ReturnAddress.County,Client.ReturnAddress.Postcode,Client.ReturnTelephone,Client.Fax,Client.DXNumber,Empty.Field112,Instruction.Introducer,Empty.Field114,Empty.Field115,Empty.Field116,Case.StatusNotes4,Case.StatusNotes5,Case.StatusNotes6,Form.DocumentDate,Empty.Field121,Instruction.Notes0,Instruction.Notes1,Instruction.Notes2,Valuation.Present,Valuation.ReinstatementCost,Valuation.AfterWorks,Valuation.PricePerSquareMeter,Empty.Field129,Surveyor.Name,Surveyor.Address.Name,Surveyor.Address.Number,Surveyor.Address.Street,Surveyor.Address.Area,Surveyor.Address.Town,Surveyor.Address.County,Surveyor.Address.Postcode,Surveyor.Telephone,Surveyor.Fax,Empty.Field140,Empty.Field141,Empty.Field142,Empty.Field143,Case.SkipList,Invoice.invoiceNumber,Invoice.raisedDate,Empty.Field147,Case.AdminNotes0,Empty.Field149,Empty.Field150,Empty.Field151,Empty.Field152,Empty.Field153,Empty.Field154,Empty.Field155,Empty.Field156,Empty.Field157,Empty.Field158,Empty.Field159,Empty.Field160,Empty.Field161,Case.EnteredByName,Case.LastModifiedBy,Empty.Field164,Case.StatusNotes0,Case.StatusNotes1,Case.StatusNotes2,Case.StatusNotes3,Case.AdminNotes5,Empty.Field170,Empty.Field171,Empty.Field172,Empty.Field173,Case.LenderReference2,Empty.Field175,Empty.Field176,Empty.Field177,Case.spnOut,Empty.Field179,Empty.Field180,Empty.Field181,Empty.Field182,Empty.Field183,Instruction.TypeDescription,Empty.Field185,Client.StaffContactName,Client.StaffContactNumber,Empty.Field188,Empty.Field189,Invoice.Address.name,Invoice.Address.number,Invoice.Address.street,Case.SpnAllocation,Empty.Field194,Empty.Field195 -Delimiter $delimiter
    
            return $theData
        }
        "santander"
        {
            [Array]$theData = Import-Csv $thePath -Header Ignore1,Ignore2,EnteredDate,Property.Address.Street,Property.Address.Town,Property.Address.County,Property.Address.Number,Property.Address.Postcode,EnteredDate2,LastModifiedDate,Instruction.RawTypeCode,Field11.ignore,Property.Address.Area,Invoice.GrossFee,Property.Valuation.CostOfWorks,Case.SysRef,Instruction.Introducer,Instruction.SubTypeCode,Invoice.FeeNotes0,Invoice.FeeCollected,Property.Vendor.DaytimeTelephone,Property.Vendor.EveningTelephone,Property.Vendor.Address.Area,Property.Vendor.Address.Town,AdditionalLoan.OriginalSurveyorName,AdditionalLoan.FirstInspectionDate,AdditionalLoan.LastInspectionDate,LenderReference2,AdditionalLoan.OriginalPurchaseDate,Property.UnexpiredTerm,Property.GroundRent,Property.Valuation.PricePerSquareMetre,EstateAgent.AccessDetails,AdditionalLoan.OutstandingAmount,Property.RawPropertyType,SpnAllocation,AdditionalLoan.Reason,Applicants0.Title,Applicants0.Initials,Property.Price,FieldBag38,Property.Valuation.AreaInformation,Error,Property.RawTenureType,EstateAgent.Name,Property.RawConstructionType,EstateAgent.Telephone,Instruction.ReceivedDate,Instruction.LoanAmount,Property.YearBuilt,Cascade.ignore,Property.Bedrooms,Property.ReceptionRooms,Property.Builder,Appointment.AppointmentDate,Appointment.ETA,Property.RawParkingType,Applicants1.Title,Applicants1.Initials,Field59.ignore,Property.Notes0,Property.Notes1,Field62.ignore,SPNID.63,Property.LocationDescription,StatusCode,Surveyor.Code,Appointment.Notes1,Property.RawConditionType,Property.Address.Name,Property.Floors,AdditionalLoan.OriginalPurchasePrice,Applicants0.CorrespondenceAddress.County,Applicants0.CorrespondenceAddress.Postcode,Applicants0.DaytimeTelephone,Instruction.Lender,Property.RawStyle,Property.Outbuildings,PreferredValuer.78,Applicants0.EveningTelephone,Appointment.TimeWindowStart,Appointment.TimeWindowEnd,Property.FloorArea,Appointment.ControlFlag,Appointment.DelayMins,FieldBag48,Appointment.Notes0,Forms0.TypedBy,Property.BlockFloorNumber,Property.BlockFloorCount,FieldBag20,Property.Vendor.Title,Property.Vendor.Initials,Property.Vendor.Surname,Instruction.SourceSystemCode,EnteredByInitials,LastModifiedBy,Invoice.ScaleFee,CompletedDate,Instruction.TypeDescription,Applicants0.Surname,Applicants1.Surname,Client.ReturnAddress.Name,Client.ReturnAddress.Number,Client.ReturnAddress.Street,Client.ReturnAddress.Area,Client.ReturnAddress.Town,Client.ReturnAddress.County,Client.ReturnAddress.Postcode,Client.ReturnTelephone,Client.Fax,Client.DXNumber,Applicants0.CorrespondenceAddress.Town,Client.ReturnName,FieldBag.SentToMSC,Instruction.Reason,Invoice.Expenses,StatementDate,D.117,StatementNo.118,Field119.119,Forms0.DocumentDate,Forms0.UpdatedDate,Instruction.Notes0,Instruction.Notes1,Instruction.Notes2,Property.Valuation.Present,Property.Valuation.ReinstatementCost,Property.Valuation.AfterWorks,Property.RawCentralHeating,FormsTrack,Surveyor.Name,Surveyor.Address.Name,Surveyor.Address.Number,Surveyor.Address.Street,Surveyor.Address.Area,Surveyor.Address.Town,Surveyor.Address.County,Surveyor.Address.Postcode,Surveyor.Telephone,Surveyor.Fax,StandardRoutingCode,FieldBag40,FieldBag50,Area.143,OrigValuer.144,OrigValDate,D.145,FieldBag33,FieldBag35,FieldBag36,FieldBag34,Client.SourceBranchName,Applicants0.CorrespondenceAddress.Number,Property.Vendor.Address.Name,Property.Vendor.Address.Number,Property.Vendor.Address.Street,FieldBag0,Invoice.FeeNotes1,FieldBag6,Client.SourceBranchTelephone,Client.SourceBranchFax,Property.Vendor.Address.County,Client.SourceBranchCode,Applicants0.CorrespondenceAddress.Area,FieldBag29,FieldBag.LinkDate,AllocationStatus,Client.IntroducingBrokerName,FieldBag9,FieldBag46,Client.ReturnCode,LastModifiedDate2,Applicants0.CorrespondenceAddress.Street,Instruction.ReceivedDate2,FieldBag51,FieldBag53,FieldBag32,FieldBag52,FieldBag37,Instruction.TypeCodePoints,Appointment.DelayReason,ApptHours,M.180,FieldBag47,Property.Valuation.Marketability,Appointment.CreatedDate,Appointment.CreatedBy,FieldBag44,Surveyor.Group,FieldBag54,Appointment.DelayDays,SpnIn,StatusNotes0,Appointment.CreatedDate2,Forms0.SignedOffDate,Forms0.SignedOffDate2,StatusNotes1,Applicants0.CorrespondenceAddress.Name,Property.Vendor.Address.Postcode,LenderReference,Field198.198,Field199.199,Property.Valuation.Notes0,Property.Valuation.Notes1,Property.Valuation.Notes2,Property.Valuation.Notes3,Property.Valuation.Notes4,Property.Valuation.Notes5,Property.Valuation.Notes6,Property.Valuation.Notes7,Property.Valuation.Notes8,Property.Valuation.Notes9,AdminNotes0,AdminNotes1,AdminNotes2,AdminNotes3,AdminNotes4,AdminNotes5,AdminNotes6,AdminNotes7,AdminNotes8,AdminNotes9,AdminNotes10,AdminNotes11,AdminNotes12,AdminNotes13,AdminNotes14,AdminNotes15,AgentRoad.244,AgentArea.245,AgentTown.246,AgentCounty.247,FieldBag12,Applicants0.MobileTelephone,Applicants0.EmailAddress,Property.Vendor.EmailAddress,Priority,Invoice.FeeReason,FieldBag31,FieldBag18,FieldBag43,FieldBag42,PriorityReason,Property.Vendor.MobileTelephone,EstateAgent.Address.Name,EstateAgent.Address.Number,EstateAgent.Address.Street,EstateAgent.Address.Area,EstateAgent.Address.Town,EstateAgent.Address.County,EstateAgent.Address.Postcode,Property.Keyholder.Address.Name,Property.Keyholder.Address.Number,Property.Keyholder.Address.Street,Property.Keyholder.Address.Area,Property.Keyholder.Address.Town,Property.Keyholder.Address.County,Property.Keyholder.Address.Postcode,Property.Keyholder.Surname,Property.Keyholder.DaytimeTelephone,EnteredByName,FieldBag45,NewBuildP.283,Client.IntroducingBrokerCode1,Client.IntroducingBrokerCode2,FieldBag4,Property.PublicSector,FieldBag7,Property.IsMundic,RetentionAmount,FieldBag23,Property.Valuation.InspectionDate,FieldBag27,Surveyor.RICSNumber,FieldBag3,FieldBag25,Failsafe.297,FailsafeStatus.298,RoutingCodes.299,FormAudit1.300,FieldBag19,AdditionalLoan.RawLastValuationType,AdditionalLoan.LastValuationAmount,Property.IsResidential,AdditionalLoan.OutstandingRetention,Property.IsNewBuild,FieldBag22,FieldBag28,Instruction.IsBuyToLet,FieldBag26,Property.Valuation.ActualRent,Property.Valuation.EstimatedRent,FieldBag39,FieldBag21,FieldBag2,Applicants0.CreditScore,Property.IsTenanted,Forms0.SurveyedBySurveyorName,Forms0.SurveyedByFirmName,FieldBag10,FieldBag11,FieldBag41,FieldBag13,FieldBag14,FieldBag15,FieldBag16,FieldBag17,FieldBag5,FieldBag1,FieldBag55,FieldBag56,Property.IsHouseInMultipleOccupancy,FieldBag8,FieldBag24,Applicants0.IsOwnershipCheckRequired,Applicants1.IsOwnershipCheckRequired,Applicants0.Address.Name,Applicants0.Address.Number,Applicants0.Address.Street,Applicants0.Address.Area,Applicants0.Address.Town,Applicants0.Address.County,Applicants0.Address.Postcode,Applicants1.Address.Name,Applicants1.Address.Number,Applicants1.Address.Street,Applicants1.Address.Area,Applicants1.Address.Town,Applicants1.Address.County,Applicants1.Address.Postcode -Delimiter $delimiter
            return $theData
        }
        default
        {
            throw "No header name $($headerName) exists within the function Import-CsvWithHeaders"
        }
    }
}

# ---------------------------------------------------------------------
# If the case database exceeds the limit then take a cut within the limit
# ---------------------------------------------------------------------
Function Get-CasesWithinLimits
{
    param(
        [Parameter(mandatory=$true)]
        [string] $casePath,
        [Parameter(mandatory=$true)]
        [bool] $tabbed = $false      
    )    
    $caseCsvSize = (Get-Content $caseDataPath).length 
    Write-Host " The CSV supplied has $($caseCsvSize) cases the limit is $($caseSizeLimit)."

    $path = [System.IO.Directory]::GetParent($casePath).FullName
    $sourceName = [System.IO.Path]::GetFileName(($casePath))
    $newCasePath = "$(Split-Path -parent $PSCommandPath)\Copy-($($($caseSizeLimit)) script limit ) $($sourceName).csv"
    Get-Content $casePath -Head $caseSizeLimit > $newCasePath 

    return $newCasePath
}

# ---------------------------------------------------------------------
# Generate a Gateway file name
# ---------------------------------------------------------------------
Function NewGatewayFileName
{
    param(
        [int] $increment,
        [System.Data.DataRow] $gatewayRule
    )
    $gatewayOutPath = $gatewayRule["OutputDirectory"] 

    if([string]::IsNullOrEmpty($gatewayRule["OutputDirectory"]))
    {
        $gatewayOutPath = Split-Path -parent $PSCommandPath
    }

    return $gatewayOutPath  + "\$([string]::Format($gatewayRule["FileName"],$increment.ToString()))"
}

# ---------------------------------------------------------------------
# Generate a Gateway flag file name
# ---------------------------------------------------------------------
Function NewGatewayFlagFile
{
    param(
        [int] $increment,
        [System.Data.DataRow] $gatewayRule
    )
    $gatewayOutPath = $gatewayRule["FileReadyDirectory"] 

    if([string]::IsNullOrEmpty($gatewayRule["FileReadyDirectory"]))
    {
        $gatewayOutPath = Split-Path -parent $PSCommandPath
    }
    return $gatewayOutPath  + "\$([string]::Format($gatewayRule["FileName"],$increment.ToString()))"
}

Function Initialise-GatewayFile
{
    param(
           [Parameter(mandatory=$false)]
        [string] $fileName
    )
    switch($Global:fileType)
    {
        
        "barclays"  #MSO
        {
            $Global:newMSOOuter = $Global:xmlOuter         
            $Global:newMSOInner =$Global:xmlInner
        }
        default
        {
            $Global:swGatewayFile = New-Object System.IO.StreamWriter($fileName)  
        }
    } 
}

Function Update-GatewayFie
{
    
    param(
        [Parameter(mandatory=$true)]
        [String] $nodeName,
        [Parameter(mandatory=$true)]
        [String] $nodeValue

    )
    switch($Global:fileType)
    {
        "barclays"
        {
            if([string]::IsNullOrWhiteSpace($nodeValue)-eq $false)
            {
                $newMSOInner.SelectSingleNode("qstInstruction/$nodeName").InnerText = $nodeValue 
            }
        }
        default
        {
            if($nodeValue -eq "EMPTY")
            {
               $nodeValue ="" 
            }
            $swGatewayFile.WriteLine("$($nodeName.TrimEnd('2'))#$nodeValue")
        }
    }       
}
Function Save-GatewayFie
{   
    param(
        [Parameter(mandatory=$true)]
        [String] $newFileName
    )
    switch($Global:fileType)
    {
        "barclays"
        {
            $newMSOOuter.Envelope.Body.SendMessage.xml.InnerXml = $newMSOInner.SelectNodes("qstInstruction").InnerXml
            $newMSOOuter.Save($newFileName)
        }
        default
        {
        }
    }       
}
Function Dispose-GatewayFie
{   
    switch($Global:fileType)
    {
        "barclays"
        {
        }
        default
        {
            $swGatewayFile.Dispose()
        }
    }       
}
# ---------------------------------------------------------------------
# Table initialise function.
# Given a CSV data source create a DataTable using the CSV column names
# ---------------------------------------------------------------------
Function Set-DataTableContentFromCSV
{
    param(
        [Parameter(mandatory=$true)]
        [String] $primaryKeyName,
        [Parameter(mandatory=$true)]
        [Array] $TheCSV,
        [Parameter(mandatory=$true)]
        [System.Data.DataTable] $TheDataTable,
        [Parameter(mandatory=$false)]
        [bool] $includeData = $true

    )
    [Array]$columnNames = @()    
    $properties = $TheCSV[0][0] | Get-Member -MemberType Properties        
        
    for($i=0; $i  -lt $properties.Count; $i++ )
    {
        $column = $properties[$i]
        $value = $run | Select -ExpandProperty $column.Name

        $TheDataTable.Columns.Add($column.Name) | Out-Null              
    }

    $TheDataTable.PrimaryKey = $TheDataTable.Columns[$primaryKeyName]
    
    if($includeData)
    {
        Set-TableFromCSV $primaryKeyName $TheCSV $TheDataTable
    }    
}

# ---------------------------------------------------------------------
# Table population function.
# Requires a CSV and DataTable columns to be named correctly.
# Given a matched Table and csv, populate the table wih the csv data
# ---------------------------------------------------------------------
Function Set-TableFromCSV
{
    param(
        [Parameter(mandatory=$true)]
        [String] $primaryKeyName,
        [Parameter(mandatory=$true)]
        [Array] $TheCSV,
        [Parameter(mandatory=$true)]
        [System.Data.DataTable] $TheDataTable
    )

    $caseSize = 0
    # for each entry within the merge run generate gateway files from merge data
    foreach($run in $TheCSV)
    {    
        $properties = $run | Get-Member -MemberType Properties
        $newRow = $TheDataTable.NewRow()
                
        for($i=0; $i  -lt $properties.Count; $i++ )
        {        
            $column = $properties[$i]
            $value = $run | Select -ExpandProperty $column.Name
            switch($column.Name)
            {
                "Length"{}
                default
                {
                    $newRow[$column.Name] = $value
                }
            }   
        }

        if($newRow[$primaryKeyName] -ne "")
        {
            $TheDataTable.Rows.Add($newRow);
        }

        $caseSize++     

        # The case data source might be quite big don't process more than the limit
        if($caseSize -gt $Global:caseRuleLimit -or $caseSize -gt $caseSizeLimit )  
        {
            break
        }
    }
}

Function Logger-Write
{
    param(
        [Parameter(mandatory=$true)]
        [string] $logEntry
    )

    if($Global:logger)
    {
        $logger.WriteLine("$(get-date -f u)`t $logEntry." ) 
    }   
    
    if($logToHost)
    {
        Write-Host "$(get-date -f u)`t $logEntry."        
    }   
}

# ---------------------------------------------------------------------
# Creates a gateway file(s) based on the provided rule table
# ---------------------------------------------------------------------
Function CreateGatewayFile
{
    param(
        [Parameter(mandatory=$true)]
        [System.Data.DataRow] $gatewayRule,
        [Parameter(mandatory=$true)]
        [System.Data.DataTable]$Mapping,
        [Parameter(mandatory=$true)]
        [System.Data.DataRow] $gatewayData,     
        [Parameter(mandatory=$true)]
        [int] $i
    )
               
    # Using the provided rule and data generate an gateway file          
    try
    {     
        
        $newFileName = $(NewGatewayFileName $i $gatewayRule )
        Initialise-GatewayFile $newFileName
   
        Logger-Write "About to format an gateway file named: $newFileName"
        Logger-Write "The file will now be populated based on the SysRef $($gatewayData["Case.SysRef"])."       

        # Iterate through the mapping
        foreach ($c in $Mapping.Columns)
        {
            # If empty column name or seed column ignore it
            if([string]::IsNullOrEmpty($c.ColumnName) -or $c -eq "SysRef")
            {
                Continue
            }
           
            switch($c.ColumnName)
            {
                "Case.LenderReference" 
                { 
                    if([String]::IsNullOrWhiteSpace($gatewayRule.Identifier) -eq $false)
                    {                                    
                        $value = [string]::Format($gatewayRule.Identifier, $i)
                    }                        
                }                
                "Property.Address.Number" 
                {
                    $value = $i
                }
                default {$value = $gatewayData[$c.ColumnName]}
            }
             
            $mapColumnName = $Mapping.Rows[0][$c.ColumnName]

            if([string]::IsNullOrWhiteSpace($mapColumnName)-eq $false -AND $mapColumnName.ToString().EndsWith('/') -eq $false )
            {
                $defaultValue="EMPTY"
                
                if([string]::IsNullOrWhiteSpace($value)-eq $false)
                {
                    $defaultValue = $value
                }

		        switch($fileType)
                {
                    "barclays"
                    {
                        if([string]::IsNullOrWhiteSpace($value)-eq $false)
                        {
                            Update-GatewayFie $mapColumnName $value 
                        }
                    }
                    default
                    {
                        Update-GatewayFie $mapColumnName $defaultValue
                    }
                }

            }                                          
        }

        Logger-Write "About to save a new gateway file named: $newFileName"
        Save-GatewayFie $newFileName 
        Logger-Write "File saved successfully named: $newFileName"    

        if($gatewayRule.SleepInMilliSeconds -gt 0)
        {          
            Logger-Write "About to sleep the sleep interval is $($gatewayRule.SleepInMilliSecond) seconds..."      
            [System.Threading.Thread]::Sleep($gatewayRule.SleepInMilliSeconds)
        }

        Logger-Write "About to Create a flag file for the gateway file $($newFileName) named: $flagFileName"
        $flagFileName = $(NewGatewayFlagFile $i $gatewayRule )
        $swGatewayFileReady = New-Object System.IO.StreamWriter($flagFileName)        
        Logger-Write "Completed creating $($i + 1) file(s) for rule ID $($gatewayRule.SysRef)"               
    }
    catch
    {
            Logger-Write $_.Exception
            throw
    }
    finally
    {                        
        Dispose-GatewayFie
        $swGatewayFileReady.Dispose()
    }    
}

Function Get-DataRowBySysRefFromCSV
{
    param(
        [Parameter(mandatory=$true)]
        [String] $primaryKeyName,
        [Parameter(mandatory=$true)]
        [System.Data.DataTable] $theDataTable,   
        [Parameter(mandatory=$true)]
        [Array] $theCSV,
        [Parameter(mandatory=$true)]
        [String] $theKey     
    )       
    foreach($case in $theCSV)
    {    
        $value = $case | Select -ExpandProperty $primaryKeyName
        
        if($theKey -eq $value)
        {
            return Get-DataRowFromCSV $primaryKeyName $theDataTable $case
        }
    }

    return $null
}

Function Get-DataRowFromCSV
{
    param(
        [Parameter(mandatory=$true)]
        [String] $primaryKeyName,
        [Parameter(mandatory=$true)]
        [System.Data.DataTable] $TheDataTable,
        [Parameter(mandatory=$true)]
        [System.Object] $case
    )

    $properties = $case | Get-Member -MemberType Properties
    $newRow = $TheDataTable.NewRow()
                
    for($i=0; $i  -lt $properties.Count; $i++ )
    {
        
        $column = $properties[$i]           
        if($column.Definition.EndsWith("=null"))
        {
            Write-Host "Found the field $($column.Name) has a null value."
            $value=""
        }  
        else
        {
            $value = $case | Select -ExpandProperty $column.Name
        }
        
        switch($column.Name)
        {
            "Length"{}
            default
            {
                $newRow[$column.Name] = $value
            }
        }   
    }

    if($newRow[$primaryKeyName] -ne "")
    {
        return ,$newRow        
    }

    return $null
}

Function Start-GenerateFilesFromRuleTable
{
    param(
    [Parameter(mandatory=$true)]
    [System.Data.DataTable]$Rules,
    [Parameter(mandatory=$true)]
    [Array]$caseCsv,
    [Parameter(mandatory=$true)]
    [System.Data.DataTable]$Mapping
    )
    Try
    {  
        $Global:xmlOuter = [xml](Get-Content ("$(Split-Path -parent $PSCommandPath)\templates\mso-outer-envelope.xml"))          
        $Global:xmlInner = [xml](Get-Content ("$(Split-Path -parent $PSCommandPath)\templates\mso-xml-template.xml")) 

        $Global:logger = New-Object System.IO.StreamWriter("$(Split-Path -parent $PSCommandPath)\gateway$(get-date -f MMddyyyy-HHmmss).log")      
        Logger-Write "The Gateway file generator script started. Gateway-Data-Generator"
        Logger-Write "Looking for the rules to generate gateway files at: $gwMergePath"
        Logger-Write "Looking for the data to generate gateway files at: $gwBasePath"
                     
        # Originaly I populated the table now just use it to generate DataRows but dont add data
        $casePrimaryKey = "Case.SysRef"
        $caseRecordsDataTable = New-Object System.Data.DataTable("caseDataSource")
        Set-DataTableContentFromCSV $casePrimaryKey $pervasivedata $caseRecordsDataTable $false
                 
        # Using the loaded rules and data files generate Gateway files.
        foreach($rule in $Rules)
        {           
            if($rule.Enabled -eq 1)
            {
                $Global:caseRuleLimit = $rule.Quantity                             
                Logger-Write "Found rule to generate gateway files, rule id is $($rule.SysRef), the rule will generate $($rule.Quantity) per datasource row."                        
                Logger-Write "Attempting to generate $($rule.Quantity) gateway files for this rule..."
                
                $index = 0
                if($rule.SysRef -eq 0)
                {
                    Logger-Write "The rule $($rule.SysRef) is a one to one rule and will generate a gateway file for each case within the datasource"
                    Logger-Write "About to genrate $($rule.Quantity) gateway files based on SysRef $($rule.SysRef)..."

                    # This is a one to one rule. Creates a gateway file for each data item
                    foreach($case in $caseCsv)                        
                    {                                                                      
                        $dataToMap = Get-DataRowFromCSV $casePrimaryKey $caseRecordsDataTable $case

                        if($dataToMap -eq $null)
                        {
                            Continue
                        }

                        CreateGatewayFile $rule $Mapping $dataToMap $index
                        $index++
                        
                        # Break out of the loop of the required quantity of cases has been generated.
                        if($caseRuleLimit -le $index)
                        {
                            break;
                        }
                    }                                                            
                }
                else
                {        
                    Logger-Write "The rule $($rule.SysRef) is a one to many rule and will generate multiple gateway files based on the SysRef $($rule.SysRef)..."
                    Logger-Write "About to genrate $($rule.Quantity) gateway files based on SysRef $($rule.SysRef)..."    
                                                
                    # Find the rule the mapping is based on                    
                    $dataToMap = Get-DataRowBySysRefFromCSV $casePrimaryKey $caseRecordsDataTable $caseCsv $rule.SysRef
                    
                    [int]$propertyNumber = $rule.PropertyNumber
                    Logger-Write "This rule will generate property numbers begining at $($rule.PropertyNumber)."

                    if($dataToMap -ne $null)
                    {
                        # This is a one to many rule. Create multiple files from a single data item
                        for($i=0; $i -lt$rule.Quantity; $i++)
                        {
                            CreateGatewayFile $rule $Mapping $dataToMap $propertyNumber
                            $propertyNumber++
                        }  
                    }
                    else
                    {
                        Logger-Write "The rule did not find a case within the case data with SysRef: $($rule.SysRef)."
                    }                  
                }
            }                 
        }

        Logger-Write "The Gateway file generation script has commpleted."
    }
    Catch
    {
        Logger-Write $_.Exception
        throw
    }    
    Finally
    {
        $Global:logger.Dispose();
    }
}
function Start-RuleBasedGatewayScript
{
    param(
        [Parameter(mandatory=$true)]
        [String] $gatewayFileFormat,
        [Parameter(mandatory=$true)]
        [int] $gatewayFileLimit,
        [Parameter(mandatory=$true)]
        [int] $CsvProcessLimit,
        [Parameter(mandatory=$true)]
        [String] $clientHeaderName,
        [Parameter(mandatory=$true)]
        [String] $clientCsvName,
        [Parameter(mandatory=$true)]
        [bool] $delimiterTabbed,
        [Parameter(mandatory=$true)]
        [String] $clientGatewayMappingName,
        [Parameter(mandatory=$true)]
        [String] $gatewayRulesName,
        [Parameter(mandatory=$false)]
        [bool] $logToHost =$false
    )

    [string]$global:headerName = $clientHeaderName.ToLower()
    [string]$Global:fileType = $gatewayFileFormat
    [int] $Global:caseSizeLimit = $CsvProcessLimit
    [int] $Global:caseRuleLimit = $gatewayFileLimit
    [bool] $Global:logToHost = $logToHost
    $basePath = $(Split-Path -parent $PSCommandPath)
    $caseDataPath ="$($basePath)\$($clientHeaderName)\$($clientCsvName)"
    $gatewayMapPath ="$($basePath)\$($clientHeaderName)\$($clientGatewayMappingName)"
    $rulesPath ="$($basePath)\$($gatewayRulesName)"
    
    # If the case data source is larger than the set limit produce a cut of the data
    $cutOfCasesPath = Get-CasesWithinLimits $caseDataPath $delimiterTabbed

    $mergeRuleTable       = New-Object System.Data.DataTable("MergeRules")
    $gwCaseMapDataTable  = New-Object System.Data.DataTable("gatewayToCaseMapping")

    # Import the data sources required to generate gateway files into the created DataTables
    $gwFileMapping    = Import-Csv $gatewayMapPath
    $rulesData     = Import-Csv $rulesPath   
    $pervasivedata = Import-CsvWithHeaders $cutOfCasesPath $delimiterTabbed  
    Set-DataTableContentFromCSV "SysRef" $rulesData $mergeRuleTable
    Set-DataTableContentFromCSV "Case.LenderReference" $gwFileMapping $gwCaseMapDataTable

    # Run the script to create Gateway files
    Start-GenerateFilesFromRuleTable $mergeRuleTable $pervasivedata $gwCaseMapDataTable
}

Export-ModuleMember -Function 'Start-RuleBasedGatewayScript'

